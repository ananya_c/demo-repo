package com.amrutsoftware.plugins.jira.mailhandler.jira.tabpanels;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amrutsoftware.plugins.jira.mailhandler.servlet.MailSendrServlet;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanel;
import com.atlassian.jira.user.ApplicationUser;
import com.sun.mail.imap.IMAPInputStream;

public class IssueWriteMailTab extends AbstractIssueTabPanel implements IssueTabPanel {
    private static final Logger log = LoggerFactory.getLogger(IssueWriteMailTab.class);

	@Override
	public List<IssueAction> getActions(Issue issue, ApplicationUser arg1) {
		// TODO Auto-generated method stub
		final String issueKey = issue.getKey();
		List<IssueAction> l = new ArrayList<IssueAction>();
		l.add(new IssueAction() {
			@Override
			public boolean isDisplayActionAllTab() {
				// TODO Auto-generated method stub
				return true;
			}
			
			@Override
			public Date getTimePerformed() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getHtml() {
				// TODO Auto-generated method stub
				String from = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getEmailAddress();
		    	String host = "imap.gmail.com";
		    	int port = 993;
		    	Properties props = new Properties();
		    	props.put("mail.imap.host", host);
		    	props.put("mail.imap.port", port);
		    	props.put("mail.imap.socketFactory.port", port);
		        props.put("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		        props.put("mail.imap.socketFactory.fallback", "false");
		        final String password = MailSendrServlet.password1;
		    	Session session = Session.getInstance(props);
		    	session.setDebug(true);
		    	try {
					Store store = session.getStore("imap");
					store.connect(from, password);
					System.out.println("\n\n\n###################################################################\n\n\nConnected\n\n\n###################################################################\n\n\n");
					Folder folderInbox = store.getFolder("INBOX");
		            folderInbox.open(Folder.READ_ONLY);
		            Message[] messages = folderInbox.getMessages();
		            IMAPInputStream inputStream = (IMAPInputStream) messages[messages.length-1].getContent(); 
		            System.out.println("\n\n\n###################################################################\n\n\nfrom : "+messages[messages.length-1].getFrom()[0].toString()+"\nsubject : "+messages[messages.length-1].getSubject()+"\nBody : "+inputStream.toString()+"\n\n\n###################################################################\n\n\n");
		            //javax.mail.Folder[] folders = store.getDefaultFolder().list("*");
		            /*for (Folder folder : folders)
		            	System.out.println("\n\n\n###################################################################\n\n\nfrom : "+folder.getFullName()+"\n\n\n###################################################################\n\n\n");*/
		            Folder folderSent = store.getFolder("[Gmail]/Sent Mail");
		            folderSent.open(Folder.READ_ONLY);
		            Message[] messages1 = folderSent.getMessages();
		            System.out.println("\n\n\n###################################################################\n\n\nTo : "+messages1[messages1.length-1].getRecipients(Message.RecipientType.TO)[0].toString()+"\nsubject : "+messages1[messages1.length-1].getSubject()+"\nBody : "+messages1[messages1.length-1].getContent().toString()+"\n\n\n###################################################################\n\n\n");
				} catch (NoSuchProviderException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return "<div class='aui-tabs horizontal-tabs' id='tabs-example1'>"+
					    "<ul class='tabs-menu'>"+
				            "<li class='menu-item active-tab'>"+
				                "<a href='#tabs-example-first'><strong><span class='aui-icon aui-icon-small aui-iconfont-devtools-clone'></span> <b>Inbox</b></strong></a>"+
				            "</li>"+
				            "<li class='menu-item'>"+
				                "<a href='#tabs-example-second'><strong><span class='aui-icon aui-icon-small aui-iconfont-devtools-pull-request'></span> <b>Sent</b></strong></a>"+
				            "</li>"+
				        "</ul>"+
				        "<div class='tabs-pane active-pane' id='tabs-example-first'><br>"+
				            "<table class='aui'>"+
				              "<tbody>"+
				                  "<tr>"+
				                      "<td headers='basic-username'>mbond</td>"+
				                  "</tr>"+
				                  "<tr>"+
				                      "<td headers='basic-username'>rchaldecott</td>"+
				                  "</tr>"+
				                  "<tr>"+
				                      "<td headers='basic-username'>htapia</td>"+
				                  "</tr>"+
				              "</tbody>"+
				            "</table>"+
				        "</div>"+
				        "<div class='tabs-pane' id='tabs-example-second'>"+
				            "<br>"+
				            "<table class='aui'>"+
				              "<tbody>"+
				                  "<tr>"+
				                      "<td headers='basic-username'>sruiz</td>"+
				                  "</tr>"+
				                  "<tr>"+
				                      "<td headers='basic-username'>scurtis</td>"+
				                  "</tr>"+
				                  "<tr>"+
				                      "<td headers='basic-username'>mwatson</td>"+
				                  "</tr>"+
				              "</tbody>"+
				            "</table>"+
				        "</div>"+
				"</div><!-- .aui-tabs --><br>"+
				"<script type='text/javascript'>"+
							
							"AJS.$('#dialog-show-button').click(function() {"+
								"AJS.dialog2('#demo-dialog').show();"+
								"console.log('Inside Cilck');"+
							"});"+
				
							"// Hides the dialog"+
							"AJS.$('#dialog-close-button').click(function(e) {"+
								"e.preventDefault();"+
								"AJS.dialog2('#demo-dialog').hide();"+
							"});"+
				
							"// Show event - this is triggered when the dialog is shown"+
							"AJS.dialog2('#demo-dialog').on('show', function() {"+
								"console.log('demo-dialog was shown');"+
							"});"+
				
							"// Hide event - this is triggered when the dialog is hidden"+
							"AJS.dialog2('#demo-dialog').on('hide', function() {"+
								"console.log('demo-dialog was hidden');"+
							"});"+
				
							"// Global show event - this is triggered when any dialog is show"+
							"AJS.dialog2.on('show', function() {"+
								"console.log('a dialog was shown');"+
							"});"+
				
							"// Global hide event - this is triggered when any dialog is hidden"+
							"AJS.dialog2.on('hide', function() {"+
								"console.log('a dialog was hidden');"+
							"});"+
								
			"</script>"+
				"<button id='dialog-show-button' class='aui-button'>Compose <span class='aui-icon aui-icon-small aui-iconfont-email-large'></span></button>"+
				"<section role='dialog' id='demo-dialog' class='aui-layer aui-dialog2 aui-dialog2-medium' aria-hidden='true'>"+
			    "<form action='"+ComponentAccessor.getApplicationProperties().getString("jira.baseurl")+"/plugins/servlet/mailsendrservlet?issueKey="+issueKey+"' method='post' id='d' class='aui'>"+
			        "<!-- Dialog header -->"+
			        "<header class='aui-dialog2-header'>"+
			            "<!-- The dialog's title -->"+
			            "<h1 class='aui-dialog2-header-main'>Write Mail</h1>"+
			            "<!-- Close icon -->"+
			            "<a class='aui-dialog2-header-close'>"+
			                "<span class='aui-icon aui-icon-small aui-iconfont-close-dialog'>Close</span>"+
			            "</a>"+
			        "</header>"+
			        "<!-- Main dialog content -->"+
			        "<div class='aui-dialog2-content'>"+
			            "<fieldset>"+
				            "<div class='field-group'>"+
			                    "<label for='d-fname'>From :</label>"+
			                    "<a id='clickable-label' class='aui-label' href='"+ComponentAccessor.getApplicationProperties().getString("jira.baseurl")+"/secure/ViewProfile.jspa'>"+ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getEmailAddress()+"</a>"+
			                "</div>"+
			                "<div class='field-group'>"+
				                "<label for='password' accesskey='p'>Password :</label>"+
				                "<input class='password' type='password' id='password' name='password' title='password'>"+
			                "</div>"+
			            	"<div class='field-group'>"+
			        			"<label for='email1'>TO :</label>"+
			        			"<input class='text long-field' type='text' id='to' name='to' title='email' placeholder='you@example.com'>"+
			        		"</div>"+
			        		"<div class='field-group'>"+
			        			"<label for='email1'>Cc :</label>"+
			        			"<input class='text long-field' type='text' id='cc' name='cc' title='email' placeholder='you@example.com'>"+
			        		"</div>"+
			        		"<div class='field-group'>"+
			        			"<label for='email1'>Bcc :</label>"+
			        			"<input class='text long-field' type='text' id='bcc' name='bcc' title='email' placeholder='you@example.com'>"+
			        		"</div>"+
			        		"<div class='field-group'>"+
			        			"<label for='d-lname'>Subject :</label>"+
			        			"<input class='text long-field' type='text' id='subject' name='subject' title='Subject'>"+
			        		"</div>"+
			        		"<div class='field-group'>"+
			        			"<label for='comment'>Body :</label>"+
			        			"<textarea class='textarea long-field' name='body' id='body' rows=6></textarea>"+
			        		"</div>"+
			            "</fieldset>"+
			        "</div>"+
			        "<!-- Dialog footer -->"+
			        "<footer class='aui-dialog2-footer'>"+
			            "<!-- Actions to render on the right of the footer -->"+
			            "<div class='aui-dialog2-footer-actions'>"+
			                "<input class='aui-button aui-button-primary' type='submit' value='Send'>"+
			            "</div>"+
			        "</footer>"+
			    "</form>"+
			"</section>";
			}
		});
		return l;
	}

	@Override
	public boolean showPanel(Issue arg0, ApplicationUser arg1) {
		// TODO Auto-generated method stub
		return true;
	}

}
