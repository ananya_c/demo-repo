package com.amrutsoftware.plugins.jira.mailhandler.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;

public class MailSendrServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(MailSendrServlet.class);
    public static String password1;

	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		password1 = req.getParameter("password");
    	String from = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getEmailAddress();
    	String host = "smtp.gmail.com";
    	int port = 465;
    	Properties props = new Properties();
    	props.put("mail.smtp.host", host);
    	props.put("mail.smtp.port", port);
    	props.put("mail.smtp.auth", true);
    	props.put("mail.smtp.ssl.enable", true);
    	props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        final String password = req.getParameter("password");
    	Authenticator authenticator = new Authenticator() {
            private PasswordAuthentication pa = new PasswordAuthentication(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getEmailAddress(), password);
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return pa;
            }
    	};
    	Session session = Session.getInstance(props, authenticator);
    	session.setDebug(true);
    	MimeMessage message = new MimeMessage(session);
    	try {
    		message.setFrom(new InternetAddress(from));
    	    InternetAddress[] address = {new InternetAddress(req.getParameter("to"))};
    	    message.setRecipients(Message.RecipientType.TO, address);
    	    message.setSubject(req.getParameter("subject"));
    	    message.setSentDate(new Date());
    	    message.setText(req.getParameter("body"));
    	    
    	    Transport.send(message, from, req.getParameter("password"));
    	} catch (MessagingException ex) {
    	    ex.printStackTrace();
    	}
        req.getRequestURI();
        resp.sendRedirect(ComponentAccessor.getApplicationProperties().getString("jira.baseurl")+"/browse/"+req.getParameter("issueKey"));
    }

}