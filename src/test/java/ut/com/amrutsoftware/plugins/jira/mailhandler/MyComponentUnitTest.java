package ut.com.amrutsoftware.plugins.jira.mailhandler;

import org.junit.Test;
import com.amrutsoftware.plugins.jira.mailhandler.api.MyPluginComponent;
import com.amrutsoftware.plugins.jira.mailhandler.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}